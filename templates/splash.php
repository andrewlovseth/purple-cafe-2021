<?php

/*

	Template Name: Splash

*/

    get_template_part('templates/splash/announcement');

    get_template_part('templates/splash/header');

    get_template_part('templates/splash/main');

    get_template_part('templates/splash/footer');

?>