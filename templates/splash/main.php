<section class="splash">
    <div class="splash-header">
        <div class="logo">
            <img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </div>
    </div>

    <div class="splash-body" style="background-image: url(<?php $image = get_field('background_image'); echo $image['url']; ?>);">
        <div class="links">
            <?php if(have_rows('buttons')): while(have_rows('buttons')): the_row(); ?>

                <?php if(get_sub_field('disable')): ?>
                    <?php 
                        $link = get_sub_field('link');
                        if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>

                        <div class="link">
                            <a href="#" class="disabled"><?php echo esc_html($link_title); ?><span class="message"><?php echo get_sub_field('message'); ?></span></a>
                        </div>

                    <?php endif; ?>
                <?php else: ?>

                    <?php 
                        $link = get_sub_field('link');
                        if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>

                        <div class="link">
                            <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        </div>

                    <?php endif; ?>

                <?php endif; ?>

            <?php endwhile; endif; ?>
        </div>
    </div>
</section>