<?php

/*

	Template Name: Home

*/

$hero_photo = get_field('hero_photo');
$top_photo = get_field('top_photo');
$bottom_left_photo = get_field('bottom_left_photo');
$bottom_right_photo = get_field('bottom_right_photo');

get_header(); ?>

    <section class="hero-photo">
        <div class="content">
            <?php echo wp_get_attachment_image($hero_photo['ID'], 'full'); ?>
        </div>
    </section>

    <section class="main">
        <div class="description">
            <?php echo get_field('description'); ?>
        </div>

        <section class="photos">
            <div class="top-photo">
                <?php echo wp_get_attachment_image($top_photo['ID'], 'full'); ?>
            </div>

            <div class="bottom-left-photo">
                <?php echo wp_get_attachment_image($bottom_left_photo['ID'], 'full'); ?>
            </div>

            <div class="bottom-right-photo">
                <?php echo wp_get_attachment_image($bottom_right_photo['ID'], 'full'); ?>
            </div>
        </section>
    </section>

<?php get_footer(); ?>