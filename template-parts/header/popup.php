<?php

    $slug = bearsmith_get_location($post);
    $popup_text = get_field($slug . '_popup_text', 'options');
    if($popup_text):

?>

    <div class="popup">
        <span class="popup-link"><span><?php echo $popup_text; ?></span></span>
    </div>
	
<?php endif; ?>