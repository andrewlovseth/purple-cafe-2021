<?php

    $slug = bearsmith_get_location($post);
    $hours_header = get_field($slug . '_hours_header', 'options');
    $hours = get_field($slug . '_hours', 'options');

?>

<div class="info left">
	<?php if($hours_header): ?>
		<h3><?php echo $hours_header; ?></h3>
	<?php else: ?>
		<h3>Hours</h3>
	<?php endif; ?>

    <p><?php echo $hours; ?></p>	
</div>