<?php

    $slug = bearsmith_get_location($post);
    $social_links = $slug . '_social_links';
?>

<?php if(have_rows($social_links, 'options')): ?>
	<div class="social-links">
		<?php while(have_rows($social_links, 'options')): the_row(); ?>

			<?php 
				$icon = get_sub_field('icon');
				$link = get_sub_field('link');

				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>

				<div class="social-link <?php echo sanitize_title_with_dashes($link_title); ?>">
					<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
						<img src="<?php echo $icon['url']; ?>" alt="<?php echo esc_html($link_title); ?>" />
					</a>
				</div>

			<?php endif; ?>


		<?php endwhile; ?>
	</div>
<?php endif; ?>