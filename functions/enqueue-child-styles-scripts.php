<?php
// Enqueue custom styles and scripts
function bearsmith_enqueue_child_styles_and_scripts() {
    $dir = get_stylesheet_directory_uri();
    wp_enqueue_style( 'purple-style', $dir . '/purple.css', '', '' );
    wp_enqueue_script( 'purple-scripts', $dir . '/js/purple.js', '', '', true );
}
add_action( 'wp_enqueue_scripts', 'bearsmith_enqueue_child_styles_and_scripts', 11);